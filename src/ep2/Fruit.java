package ep2;

import java.awt.Color;
import java.util.Random;

public class Fruit {
	
	private int xCoordinate;
	private int yCoordinate;
	protected Color color = Color.green;
	protected int scorePoints = 1;
	
	public Fruit() {
		xCoordinate = -1;
		yCoordinate = -1;
	}
	
	public Fruit(int maxX, int maxY, int x, int y) {
		while(true) {
			Random randomX = new Random();
			Random randomY = new Random();
			xCoordinate = randomX.nextInt(maxX);
			yCoordinate = randomY.nextInt(maxY);
			if(xCoordinate != x || yCoordinate != y)
				break;
		}
	}

	public void setXCoordinate(int x) {
		xCoordinate = x;
	}
	
	public int getXCoordinate() {
		return xCoordinate;
	}
	
	public void setYCoordinate(int y) {
		yCoordinate = y;
	}
	
	public int getYCoordinate() {
		return yCoordinate;
	}
	
	public Color getColor() {
		return color;
	}
	
	public int getScorePoints() {
		return scorePoints;
	}
	
	public void changeSnake(Snake snake) {
	}
	
}
