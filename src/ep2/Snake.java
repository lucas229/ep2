package ep2;

import java.awt.Color;
import java.util.ArrayList;

public class Snake {

	private ArrayList <Integer> xCoordinates = new ArrayList <> ();
	private ArrayList <Integer> yCoordinates = new ArrayList <> ();
	private int length;
	private String direction;
	private int pointWeight;
	private Color color;
	private boolean alive;
	private boolean breakBarrier;
	
	public Snake() {
		xCoordinates.add(8);
		yCoordinates.add(7);
		length = 1;
		direction = "";
		pointWeight = 1;
		color = Color.green;
		alive = true;
		breakBarrier = false;
	}
	
	public int getXCoordinate(int index) {
		return xCoordinates.get(index);
	}
	
	public int getYCoordinate(int index) {
		return yCoordinates.get(index);
	}
	
	public void setXCoordinate(int index, int coordinate) {
		if(index >= xCoordinates.size())
			xCoordinates.add(coordinate);
		else
			xCoordinates.set(index, coordinate);
	}
	
	public void setYCoordinate(int index, int coordinate) {
		if(index >= yCoordinates.size())
			yCoordinates.add(coordinate);
		else
			yCoordinates.set(index, coordinate);	
	}
	
	public void setDirection(String direction) {
		this.direction = direction;
	}
	
	public String getDirection() {
		return direction;
	}
	
	public void setLength(int length) {
		this.length = length;
	}
	
	public int getLength() {
		return length;
	}
	
	public void setPointWeight(int weight) {
		pointWeight = weight;
	}
	
	public int getPointWeight() {
		return pointWeight;
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	public boolean getAlive() {
		return alive;
	}
	
	public void setAlive(boolean alive) {
		this.alive = alive;
	}
	
	public void setBreakBarrier(boolean breakBarrier) {
		this.breakBarrier = breakBarrier;
	}
	
	public boolean getBreakBarrier() {
		return breakBarrier;
	}
	
	public void resetSnake() {
		xCoordinates.subList(1, xCoordinates.size()).clear();
		yCoordinates.subList(1, yCoordinates.size()).clear();
		length = 1;
	}

}
