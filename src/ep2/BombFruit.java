package ep2;

import java.awt.Color;

public class BombFruit extends Fruit {

	public BombFruit() {
		color = Color.red;
		scorePoints = 0;
	}
	
	@Override
	public void changeSnake(Snake snake) {
		snake.setAlive(false);
	}
	
}
