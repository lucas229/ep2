package ep2;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileManager {
		
	public void saveFile(String playerName, String snakeType, int score) {
		checkFiles();
		try {
			FileReader inFile = new FileReader("data" + File.separator + "ranking.txt");
			if(inFile.read() == -1) {
				FileWriter toFile = new FileWriter("data" + File.separator + "ranking.txt");
				toFile.write(playerName + "," + snakeType + "," + score + "\n");
				inFile.close();
				toFile.close();
				return;
			}
			FileWriter toFile = new FileWriter("data" + File.separator + "tmp.txt");
			inFile.close();
			inFile = new FileReader("data" + File.separator + "ranking.txt");
		    int c;
		    char ch;
		    int achei = 0;
		    String number = "";
		    String line = "";
		    boolean newScoreWritten = false;
		    while(true) {
		        c = inFile.read();
		        if(c == -1)
		        	break;
		        ch = (char)c;
		        line += ch;
		        if(ch == '\n') {
		        	achei = 0;
		        	if(Integer.parseInt(number) < score && newScoreWritten == false) {
						toFile.write(playerName + "," + snakeType + "," + score + "\n");
						newScoreWritten = true;
		        	}
					toFile.write(line);
					line = "";
					number = "";
		        }
		        if(achei == 2)
		        	number += ch;
		        if(ch == ',')
		        	achei++;
		    }
		    if(newScoreWritten == false)
				toFile.write(playerName + "," + snakeType + "," + score + "\n");
		    inFile.close();
		    toFile.close();
		    replaceFiles();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void replaceFiles() {
	    File file = new File("data" + File.separator + "ranking.txt");
	    if(file.delete()) {
	    	System.out.println(file + " deleted");
	    	File updatedFile = new File("data" + File.separator + "tmp.txt");
	    	if(updatedFile.renameTo(file))
	    		System.out.println(updatedFile + " renamed to " + file);
	    	if(updatedFile.delete())
	    		System.out.println(updatedFile + " deleted");
	    }
	}

	public void checkFiles() {
		try {
			File file = new File("data" + File.separator + "ranking.txt");
			if(!file.exists()) {
				if(file.createNewFile())
					System.out.println(file + " created");
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public int getNumberOfLines() {
		int number = 0;
		try {
			FileReader inFile = new FileReader("data" + File.separator + "ranking.txt");
			int c;
			char ch;
			while(true) {
				c = inFile.read();
				ch = (char)c;
				if(c == -1)
					break;
				if(ch == '\n')
					number++;
			}
			inFile.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
		return number;
	}
	
	public String[][] printData() {
		String[][] ranking = new String[getNumberOfLines()][4];
		try {
			FileReader inFile = new FileReader("data" + File.separator + "ranking.txt");
			char ch;
			String name = "";
			String snake = "";
			String score = "";
			int achei = 0;
			int last = 0;
			while(true) {
		        int c = inFile.read();
		        if(c == -1)
		        	break;
		        ch = (char)c;
		        if(c == '\n') {
		        	ranking[last][0] = String.valueOf(last + 1);
		        	ranking[last][1] = name;
		        	ranking[last][2] = snake;
		        	ranking[last][3] = score;
		        	last++;
		        	name = "";
		        	snake = "";
		        	score = "";
		        	achei = 0;
		        	continue;
		        }
		        if(achei == 0 && ch != ',')
		        	name += ch;
		        else if(achei == 1 && ch != ',')
		        	snake += ch;
		        else if(achei == 2 && ch != ',')
		        	score += ch;
		        if(ch == ',')
		        	achei++;
			}
			inFile.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
		return ranking;
	}
	
}
