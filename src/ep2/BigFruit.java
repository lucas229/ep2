package ep2;

import java.awt.Color;

public class BigFruit extends Fruit {

	public BigFruit() {
		color = Color.orange;
		scorePoints = 2;
	}
	
	@Override
	public void changeSnake(Snake snake) {
		snake.setLength(snake.getLength() + 1);
	}
	
}
