package ep2;

public class GameThread extends Thread {

	private GameInterface gameInterface;
	private boolean running;
	private int counter;
	
	public GameThread(GameInterface gameInterface) {
		this.gameInterface = gameInterface;
	}
	
	public void setRunning(boolean status) {
		running = status;
	}
	
	public boolean getRunning() {
		return running;
	}
	
	public void gameOperations() {
		gameInterface.moveSnake();
		gameInterface.changeBorder();
		gameInterface.checkFruit();
		gameInterface.checkSpecialFruit();
		gameInterface.repaint();
		gameInterface.revalidate();
	}
	
	public void enableSpecialFruit() {
		if(counter == 200)
			gameInterface.setSpecialFruit(new BigFruit());
		else if(counter == 400)
			gameInterface.setSpecialFruit(new BombFruit());
		else if(counter == 600 && gameInterface.getSnakeLength() >= GameInterface.getNUMBER_OF_SQUARES_X() * GameInterface.getNUMBER_OF_SQUARES_Y() * 0.05)
			gameInterface.setSpecialFruit(new DecreaseFruit());
		else if(counter == 600 || counter == 800) {
			gameInterface.setDrawSpecialFruit(false);
			gameInterface.hideFruitCoordinates();
			counter = 0;
		}
	}
	
	public boolean gameOverChecker() {
		if(gameInterface.endGame() == true) {
			gameInterface.setGameOver(true);
			try {
				synchronized(this) {
					wait();
				}
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}
	
	@Override
	public void run() {
		counter = 1;
		gameInterface.setFirstExecution(true);
		running = false;
		gameInterface.setBarriers(5);
		try {
			synchronized(this) {
				wait();
			}
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		while (true) {
			gameInterface.setKeyStatus(false);
			enableSpecialFruit();
			gameOperations();
			if(gameOverChecker())
				break;
			try {
				Thread.sleep(100);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
			counter++;
			gameInterface.setFirstExecution(false);
		}
	}
	
}
