package ep2;

import java.util.Random;

public class Barrier {

	private int[] xCoordinates;
	private int[] yCoordinates;
	private int length;
	
	public int getXCoordinate(int index) {
		return xCoordinates[index];
	}
	
	public int getYCoordinate(int index) {
		return yCoordinates[index];
	}
	
	public int getLength() {
		return length;
	}
	
	public void setHorizontalBarrier(int length, int maxX, int maxY) {
		xCoordinates = new int[length];
		yCoordinates = new int[length];
		this.length = length;
		Random x = new Random();
		Random y = new Random();
		xCoordinates[0] = x.nextInt(maxX);
		yCoordinates[0] = y.nextInt(maxY);
		for(int i = 1; i < length; i++) {
			xCoordinates[i] = xCoordinates[i - 1];
			yCoordinates[i] = yCoordinates[i - 1] + 1;
			if(yCoordinates[i] > maxY - 1)
				yCoordinates[i] = 0;
		}
	}
	
	public void setVerticalBarrier(int length, int maxX, int maxY) {
		xCoordinates = new int[length];
		yCoordinates = new int[length];
		this.length = length;
		Random x = new Random();
		Random y = new Random();
		xCoordinates[0] = x.nextInt(maxX);
		yCoordinates[0] = y.nextInt(maxY);
		for(int i = 1; i < length; i++) {
			xCoordinates[i] = xCoordinates[i - 1] + 1;
			yCoordinates[i] = yCoordinates[i - 1];
			if(xCoordinates[i] > maxX - 1)
				xCoordinates[i] = 0;
		}
	}
	
}
