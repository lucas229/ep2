package ep2;

import java.awt.Color;

public class DecreaseFruit extends Fruit {

	public DecreaseFruit() {
		color = Color.blue;
		scorePoints = 0;
	}
	
	@Override
	public void changeSnake(Snake snake) {
		snake.resetSnake();
	}
	
}
