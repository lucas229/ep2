package ep2;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Color;
import java.awt.Font;
import java.util.Random;

public class GameInterface extends JPanel {
	
	private static final int NUMBER_OF_SQUARES_X = 18;
	private static final int NUMBER_OF_SQUARES_Y = 15;
	private static final int SQUARE_LENGTH = 28;
	private int score;
	private Snake snake;
	private Fruit fruit = new Fruit(NUMBER_OF_SQUARES_X, NUMBER_OF_SQUARES_Y, 8, 7);
	private Fruit specialFruit = new Fruit();
	private boolean keyStatus;
	private boolean drawSpecialFruit;
	private boolean gameOver;
	private Barrier horizontalBarrier = new Barrier();
	private Barrier verticalBarrier = new Barrier();
	private boolean firstExecution;
	
	public GameInterface(Snake snake) {
		score = 0;
		this.snake = snake;
		keyStatus = false;
		drawSpecialFruit = false;
		gameOver = false;
		firstExecution = true;
	}
	
	public static int getNUMBER_OF_SQUARES_X() {
		return NUMBER_OF_SQUARES_X;
	}
	
	public static int getNUMBER_OF_SQUARES_Y() {
		return NUMBER_OF_SQUARES_Y;
	}
	
	public static int getSQUARE_LENGTH() {
		return SQUARE_LENGTH;
	}
	
	public void setKeyStatus(boolean status) {
		keyStatus = status;
	}
	
	public boolean getKeyStatus() {
		return keyStatus;
	}
	
	public void setDrawSpecialFruit(boolean status) {
		drawSpecialFruit = status;
	}
	
	public int getSnakeLength() {
		return snake.getLength();
	}
	
	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}
	
	public boolean getGameOver() {
		return gameOver;
	}
	
	public void setFirstExecution(boolean firstExecution) {
		this.firstExecution = firstExecution;
	}
	
	public int getScore() {
		return score;
	}
	
	public void moveSnake() {
		for(int i = snake.getLength(); i > 0; i--) {
			snake.setXCoordinate(i, snake.getXCoordinate(i-1));
			snake.setYCoordinate(i, snake.getYCoordinate(i-1));
		}
		if(snake.getDirection() == "UP")
			snake.setYCoordinate(0, snake.getYCoordinate(0) - 1);
		else if(snake.getDirection() == "DOWN")
			snake.setYCoordinate(0, snake.getYCoordinate(0) + 1);
		else if(snake.getDirection() == "LEFT")
			snake.setXCoordinate(0, snake.getXCoordinate(0) - 1);
		else if(snake.getDirection() == "RIGHT")
			snake.setXCoordinate(0, snake.getXCoordinate(0) + 1);
	}
	
	public void changeBorder() {
		if(snake.getXCoordinate(0) < 0)
			snake.setXCoordinate(0, NUMBER_OF_SQUARES_X - 1);
		else if(snake.getXCoordinate(0) > NUMBER_OF_SQUARES_X - 1)
			snake.setXCoordinate(0, 0);
		else if(snake.getYCoordinate(0) < 0)
			snake.setYCoordinate(0, NUMBER_OF_SQUARES_Y - 1);
		else if(snake.getYCoordinate(0) > NUMBER_OF_SQUARES_Y - 1)
			snake.setYCoordinate(0, 0);
	}
	
	public boolean snakeHitItself() {
		for(int i = 1; i < snake.getLength(); i++) {
			if(snake.getXCoordinate(0) == snake.getXCoordinate(i) && snake.getYCoordinate(0) == snake.getYCoordinate(i))
				return true;
		}
		return false;
	}
	
	public void checkFruit() {
		if (snake.getXCoordinate(0) == fruit.getXCoordinate() && snake.getYCoordinate(0) == fruit.getYCoordinate()) {
			score += snake.getPointWeight();
			if(snake.getLength() != NUMBER_OF_SQUARES_X * NUMBER_OF_SQUARES_Y - 1 - horizontalBarrier.getLength() * 2 || drawSpecialFruit == false)
				setFruitCoordinates(fruit, specialFruit.getXCoordinate(), specialFruit.getYCoordinate());
			snake.setLength(snake.getLength() + 1);
		}
	}
	
	public void checkSpecialFruit() {
		if(snake.getXCoordinate(0) == specialFruit.getXCoordinate() && snake.getYCoordinate(0) == specialFruit.getYCoordinate()) {
			score += specialFruit.getScorePoints() * snake.getPointWeight();
			specialFruit.changeSnake(snake);
			drawSpecialFruit = false;
			hideFruitCoordinates();
		}
	}
	
	public void setSpecialFruit(Fruit fruit) {
		specialFruit = fruit;
		if(snake.getLength() != NUMBER_OF_SQUARES_X * NUMBER_OF_SQUARES_Y - 1 - horizontalBarrier.getLength() * 2) {
			drawSpecialFruit = true;
			setFruitCoordinates(specialFruit, fruit.getXCoordinate(), fruit.getYCoordinate());
		}
	}
	
	public void setFruitCoordinates(Fruit fruit, int x, int y) {
		Random randomX = new Random();
		Random randomY = new Random();
		boolean achei = true;
		while(achei) {
			fruit.setXCoordinate(randomX.nextInt(NUMBER_OF_SQUARES_X));
			fruit.setYCoordinate(randomY.nextInt(NUMBER_OF_SQUARES_Y));
			if((fruit.getXCoordinate() == x && fruit.getYCoordinate() == y) || fruitSnakeComparator(fruit, snake))
				continue;
			if(fruitBarrierComparator(fruit, horizontalBarrier) || fruitBarrierComparator(fruit, verticalBarrier))
				continue;
			achei = false;
		}
	}
	
	public void hideFruitCoordinates() {
		specialFruit.setXCoordinate(-1);
		specialFruit.setYCoordinate(-1);
	}
	
	public boolean endGame() {
		boolean end = false;
		if(snake.getAlive() == false || snake.getLength() == NUMBER_OF_SQUARES_X * NUMBER_OF_SQUARES_Y - horizontalBarrier.getLength() * 2 || snakeHitItself())
			end = true;
		if(snake.getBreakBarrier() == false) {
			if(snakeBarrierComparator(snake, horizontalBarrier) || snakeBarrierComparator(snake, verticalBarrier))
				end = true;
		}
		return end;
	}
	
	public boolean fruitSnakeComparator(Fruit fruit, Snake snake) {
		for(int i = 0; i < snake.getLength(); i++) {
			if (fruit.getXCoordinate() == snake.getXCoordinate(i) && fruit.getYCoordinate() == snake.getYCoordinate(i)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean fruitBarrierComparator(Fruit fruit, Barrier barrier) {
		for(int i = 0; i < barrier.getLength(); i++) {
			if(barrier.getXCoordinate(i) == fruit.getXCoordinate() && barrier.getYCoordinate(i) == fruit.getYCoordinate())
				return true;
		}
		return false;
	}
	
	public boolean snakeBarrierComparator(Snake snake, Barrier barrier) {
		for(int i = 0; i < barrier.getLength(); i++) {
			if(snake.getXCoordinate(0) == barrier.getXCoordinate(i) && snake.getYCoordinate(0) == barrier.getYCoordinate(i))
				return true;
		}
		return false;
	}
	
	public boolean barriersComparator(Barrier b1, Barrier b2) {
		for(int i = 0; i < horizontalBarrier.getLength(); i++) {
			for(int j = 0; j < verticalBarrier.getLength(); j++) {
				if(horizontalBarrier.getXCoordinate(i) == verticalBarrier.getXCoordinate(j) && horizontalBarrier.getYCoordinate(i) == verticalBarrier.getYCoordinate(j))
					return true;
			}
		}
		return false;
	}
	
	public void setBarriers(int length) {
		boolean achei = true;
		while(achei) {
			horizontalBarrier.setHorizontalBarrier(length, NUMBER_OF_SQUARES_X, NUMBER_OF_SQUARES_Y);
			verticalBarrier.setVerticalBarrier(length, NUMBER_OF_SQUARES_X, NUMBER_OF_SQUARES_Y);
			if(snakeBarrierComparator(snake, horizontalBarrier) || snakeBarrierComparator(snake, verticalBarrier) || barriersComparator(horizontalBarrier, verticalBarrier) || fruitBarrierComparator(fruit, horizontalBarrier) || fruitBarrierComparator(fruit, verticalBarrier))
				continue;
			achei = false;
		}
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.black);
		g.fillRect(0, 0, NUMBER_OF_SQUARES_X * SQUARE_LENGTH, NUMBER_OF_SQUARES_Y * SQUARE_LENGTH);
		paintBarriers(g);
		paintSnake(g);
		paintFruits(g);
		paintScore(g);
		paintGameOver(g);
		paintDirections(g);
	}
	
	public void paintBarriers(Graphics g) {
        for(int i = 0; i < horizontalBarrier.getLength(); i++) {
        	g.setColor(Color.red);
        	g.fillRect(horizontalBarrier.getXCoordinate(i) * SQUARE_LENGTH, horizontalBarrier.getYCoordinate(i) * SQUARE_LENGTH, SQUARE_LENGTH, SQUARE_LENGTH);
        	g.fillRect(verticalBarrier.getXCoordinate(i) * SQUARE_LENGTH, verticalBarrier.getYCoordinate(i) * SQUARE_LENGTH, SQUARE_LENGTH, SQUARE_LENGTH);
        	g.setColor(Color.white);
        	g.drawRect(horizontalBarrier.getXCoordinate(i) * SQUARE_LENGTH, horizontalBarrier.getYCoordinate(i) * SQUARE_LENGTH, SQUARE_LENGTH, SQUARE_LENGTH);
        	g.drawRect(verticalBarrier.getXCoordinate(i) * SQUARE_LENGTH, verticalBarrier.getYCoordinate(i) * SQUARE_LENGTH, SQUARE_LENGTH, SQUARE_LENGTH);
        }
	}
	
	public void paintSnake(Graphics g) {
		for (int i = 0; i < snake.getLength(); i++) {
			g.setColor(Color.white);
			g.fillRect(snake.getXCoordinate(i) * SQUARE_LENGTH, snake.getYCoordinate(i) * SQUARE_LENGTH, SQUARE_LENGTH, SQUARE_LENGTH);
			g.setColor(snake.getColor());
			g.drawRect(snake.getXCoordinate(i) * SQUARE_LENGTH, snake.getYCoordinate(i) * SQUARE_LENGTH, SQUARE_LENGTH, SQUARE_LENGTH);
		}
		g.fillRect(snake.getXCoordinate(0) * SQUARE_LENGTH, snake.getYCoordinate(0) * SQUARE_LENGTH, SQUARE_LENGTH, SQUARE_LENGTH);
	}
	
	public void paintFruits(Graphics g) {
		g.setColor(fruit.getColor());
		g.fillOval(fruit.getXCoordinate() * SQUARE_LENGTH, fruit.getYCoordinate() * SQUARE_LENGTH, SQUARE_LENGTH, SQUARE_LENGTH);
		g.setColor(Color.white);
		g.drawOval(fruit.getXCoordinate() * SQUARE_LENGTH, fruit.getYCoordinate() * SQUARE_LENGTH, SQUARE_LENGTH, SQUARE_LENGTH);
		if(drawSpecialFruit == true) {
			g.setColor(specialFruit.getColor());
			g.fillOval(specialFruit.getXCoordinate() * SQUARE_LENGTH, specialFruit.getYCoordinate() * SQUARE_LENGTH, SQUARE_LENGTH, SQUARE_LENGTH);
			g.setColor(Color.white);
			g.drawOval(specialFruit.getXCoordinate() * SQUARE_LENGTH, specialFruit.getYCoordinate() * SQUARE_LENGTH, SQUARE_LENGTH, SQUARE_LENGTH);
		}
	}
	
	public void paintScore(Graphics g) {
		g.setColor(Color.black);
	    g.setFont(new Font("Serif", Font.BOLD, 20));
        g.drawString("Score: " + (score), 0, NUMBER_OF_SQUARES_Y * SQUARE_LENGTH + 16);
	}
	
	public void paintGameOver(Graphics g) {
        if(gameOver == true) {
           	g.setColor(Color.lightGray);
    	    g.setFont(new Font("Serif", Font.BOLD, 40));
        	g.drawString("Game Over", 150, 200);
    	    g.setFont(new Font("Serif", Font.BOLD, 30));
        	g.drawString("Aperte espa�o para continuar", 65, 230);
        }
	}
	
	public void paintDirections(Graphics g) {
        if(firstExecution) {
    		ImageIcon directionsImage = new ImageIcon("images/directions.png");
    		Image img = directionsImage.getImage();
    		g.drawImage(img, 140, 80, 202, 97, null);
        }
	}

}