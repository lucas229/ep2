package ep2;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import java.awt.EventQueue;

public class InterfacePrincipal extends JFrame implements ActionListener, KeyListener {

	private static GameInterface gameInterface;
	private GameThread gameThread;
	private Snake snake;
	private FileManager fileManager = new FileManager();
	private String snakeType;
	private String playerName;
	JTextField userName = new JTextField();
	JLabel userMessage = new JLabel();
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfacePrincipal frame = new InterfacePrincipal();
					frame.setVisible(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public InterfacePrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Snake");
		setSize(GameInterface.getNUMBER_OF_SQUARES_X() * GameInterface.getSQUARE_LENGTH() + 16, GameInterface.getNUMBER_OF_SQUARES_Y() * GameInterface.getSQUARE_LENGTH() + 57);
		setLocationRelativeTo(null);
		addKeyListener(this);
		mainMenuBuilder();
	}
	
	public void mainMenuBuilder() {
		JPanel startMenu = new JPanel();
		JButton startButton = new JButton("START");
		JButton rankingButton = new JButton("Ver ranking");
		JLabel gameName = new JLabel("SNAKE");
		String snakes[] = {"Snake Comum", "Snake Kitty", "Snake Star"};
		JComboBox <String> snakeOptions = new JComboBox <> (snakes);
		startMenu.setBackground(Color.black);
		startMenu.setLayout(new GridBagLayout());
		startMenu.setFocusable(false);
		startButton.setFocusable(false);
		startButton.addActionListener(this);
		gameName.setFont(new Font("Serif", Font.ITALIC, 60));
		gameName.setForeground(Color.white);
		userMessage.setFont(new Font("Serif", Font.ITALIC, 25));
		userMessage.setForeground(Color.white);
		userMessage.setText("Insira seu nome");
		snakeOptions.setFocusable(false);
		GridBagConstraints constraint = new GridBagConstraints();
		constraint.fill = GridBagConstraints.HORIZONTAL;
		constraint.anchor = GridBagConstraints.PAGE_START;
		constraint.weighty = 8;
		startMenu.add(gameName, constraint);
		constraint.gridy = 1;
		startMenu.add(rankingButton, constraint);
		constraint.gridy = 2;
		constraint.weighty = 0;
		startMenu.add(userMessage, constraint);
		constraint.gridy = 3;
		constraint.weighty = 5;
		startMenu.add(userName, constraint);
		constraint.gridy = 4;
		constraint.weighty = 5;
		startMenu.add(snakeOptions, constraint);
		constraint.gridy = 5;
		startMenu.add(startButton, constraint);
		setContentPane(startMenu);
		revalidate();
		snakeType = "Snake Comum";
		snake = new Snake();
		rankingButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fileManager.checkFiles();
				showRanking();
			}
		});
		snakeOptions.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(snakeOptions.getSelectedItem() == "Snake Kitty") {
					snake.setColor(Color.blue);
					snake.setBreakBarrier(true);
					snakeType = "Snake Kitty";
				} else if(snakeOptions.getSelectedItem() == "Snake Star") {
					snake.setPointWeight(2);
					snake.setColor(Color.orange);
					snakeType = "Snake Star";
				}
			}
		});
	}
	
	public void showRanking() {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.setBackground(Color.black);
		String[] headers = {"Posi��o", "Nome", "Snake", "Score"};
		JTable table = new JTable(fileManager.printData(), headers);
		table.setEnabled(false);
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		for(int i = 0; i < 4; i++)
			table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		JButton returnButton = new JButton("Voltar");
		panel.add(table, BorderLayout.NORTH);
		panel.add(returnButton, BorderLayout.SOUTH);
	    panel.add(new JScrollPane(table));
		setContentPane(panel);
		revalidate();
		returnButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mainMenuBuilder();
			}
		});
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		playerName = userName.getText();
		for(int i = 0; i < playerName.length(); i++) {
			char c = playerName.charAt(i);
			if((int)c == -1)
				break;
			if(c == ',') {
				userMessage.setText("Nome inv�lido");
				return;
			}
		}
		gameInterface = new GameInterface(snake);
		gameThread = new GameThread(gameInterface);
		setContentPane(gameInterface);
		revalidate();
		requestFocus();
		gameThread.start();
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		gameOverMessage(key);
		if(gameInterface.getKeyStatus() == true || (key == KeyEvent.VK_UP && snake.getDirection() == "DOWN") || (key == KeyEvent.VK_DOWN && snake.getDirection() == "UP") || (key == KeyEvent.VK_LEFT && snake.getDirection() == "RIGHT") || key == KeyEvent.VK_RIGHT && snake.getDirection() == "LEFT")
			return;
		changeSnakeDirection(key);
		gameStart(key);
	}
	
	@Override
	public void keyReleased (KeyEvent e) {
	}
	
	@Override
	public void keyTyped (KeyEvent e) {
	}
	
	public void gameOverMessage(int key) {
		if(key == KeyEvent.VK_SPACE && gameInterface.getGameOver() == true) {
			synchronized(gameThread) {
				gameThread.notify();
			}
			if(gameInterface.getScore() > 0)
				fileManager.saveFile(playerName, snakeType, gameInterface.getScore());
			mainMenuBuilder();
		}
	}
	
	public void changeSnakeDirection(int key) {
		if(key == KeyEvent.VK_UP) {
			snake.setDirection("UP");
			gameInterface.setKeyStatus(true);
		} else if(key == KeyEvent.VK_DOWN) {
			snake.setDirection("DOWN");
			gameInterface.setKeyStatus(true);
		} else if(key == KeyEvent.VK_LEFT) {
			snake.setDirection("LEFT");
			gameInterface.setKeyStatus(true);
		} else if(key == KeyEvent.VK_RIGHT) {
			snake.setDirection("RIGHT");
			gameInterface.setKeyStatus(true);
		}
	}
	
	public void gameStart(int key) {
		if(gameThread.getRunning() == false) {
			if(key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN || key == KeyEvent.VK_LEFT || key == KeyEvent.VK_RIGHT) {
				gameThread.setRunning(true);
				synchronized(gameThread) {
					gameThread.notify();
				}
			}
		}
	}

}