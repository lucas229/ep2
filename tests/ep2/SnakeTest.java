package ep2;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SnakeTest {

	private Snake snake;
	
	@BeforeEach
	public void beforeTests() {
		snake = new Snake();
	}
	
	@Test
	public void testSnake() {
		assertEquals(8, snake.getXCoordinate(0));
		assertEquals(7, snake.getYCoordinate(0));
		assertEquals(1, snake.getLength());
		assertEquals("", snake.getDirection());
		assertEquals(true, snake.getAlive());
	}

	@Test
	public void testSetXCoordinate() {
		snake.setXCoordinate(10, 9);
		assertEquals(9, snake.getXCoordinate(1));
	}

	@Test
	public void testSetYCoordinate() {
		snake.setYCoordinate(10, 8);
		assertEquals(8, snake.getYCoordinate(1));
	}

	@Test
	public void testResetSnake() {
		int x = snake.getXCoordinate(0);
		int y = snake.getYCoordinate(0);
		for(int i = 1; i <= 5; i++) {
			snake.setXCoordinate(i, snake.getXCoordinate(i - 1) + 1);
			snake.setYCoordinate(i, snake.getYCoordinate(i - 1));
			snake.setLength(snake.getLength() + 1);
		}
		snake.resetSnake();
		assertEquals(x, snake.getXCoordinate(0));
		assertEquals(y, snake.getYCoordinate(0));
		assertEquals(1, snake.getLength());
	}

}
