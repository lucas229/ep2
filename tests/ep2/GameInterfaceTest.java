package ep2;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

class GameInterfaceTest {
	
	GameInterface gameInterface;
	Snake snake;
	Fruit fruit;
	Barrier barrier1;
	Barrier barrier2;

	@BeforeEach
	public void beforeTests() {
		snake = new Snake();
		gameInterface = new GameInterface(snake);
	}
	
	@Test
	public void testMoveSnake() {
		snake.setXCoordinate(0, 5);
		snake.setYCoordinate(0, 8);
		snake.setDirection("LEFT");
		gameInterface.moveSnake();
		assertEquals(4, snake.getXCoordinate(0));
		assertEquals(8, snake.getYCoordinate(0));
	}

	@Test
	public void testSnakeHitItself() {
		for(int i = 0; i < 5; i++) {
			snake.setXCoordinate(i, i);
			snake.setYCoordinate(i, 2);
		}
		snake.setLength(5);
		snake.setXCoordinate(0, snake.getXCoordinate(4));
		snake.setYCoordinate(0, snake.getYCoordinate(4));
		assertEquals(true, gameInterface.snakeHitItself());
	}

	@Test
	public void testChangeBorder() {
		snake.setXCoordinate(0, GameInterface.getNUMBER_OF_SQUARES_X());
		snake.setYCoordinate(0, 4);
		gameInterface.changeBorder();
		assertEquals(0, snake.getXCoordinate(0));
		assertEquals(4, snake.getYCoordinate(0));
	}

	@Test
	public void testFruitSnakeComparator() {
		fruit = new Fruit();
		snake.setXCoordinate(0, fruit.getXCoordinate());
		snake.setYCoordinate(0, fruit.getYCoordinate());
		assertEquals(true, gameInterface.fruitSnakeComparator(fruit, snake));
	}

	@Test
	public void testSetFruitCoordinates() {
		fruit = new Fruit();
		gameInterface.setFruitCoordinates(fruit, 3, 4);
		assertNotEquals(fruit.getXCoordinate(), 3);
		assertNotEquals(fruit.getYCoordinate(), 4);
	}

	@Test
	public void testSnakeBarrierComparator() {
		barrier1 = new Barrier();
		barrier1.setVerticalBarrier(4, GameInterface.getNUMBER_OF_SQUARES_X(), GameInterface.getNUMBER_OF_SQUARES_Y());
		snake.setXCoordinate(0, barrier1.getXCoordinate(2));
		snake.setYCoordinate(0, barrier1.getYCoordinate(2));
		assertEquals(true, gameInterface.snakeBarrierComparator(snake, barrier1));
	}

	@Test
	public void testFruitBarrierComparator() {
		fruit = new Fruit();
		barrier1 = new Barrier();
		barrier1.setHorizontalBarrier(3, GameInterface.getNUMBER_OF_SQUARES_X(), GameInterface.getNUMBER_OF_SQUARES_Y());
		fruit.setXCoordinate(barrier1.getXCoordinate(0));
		fruit.setYCoordinate(barrier1.getYCoordinate(0));
		assertEquals(true, gameInterface.fruitBarrierComparator(fruit, barrier1));
	}

	@Test
	public void testBarriersComparator() {
		barrier1 = new Barrier();
		barrier2 = new Barrier();
		barrier1.setHorizontalBarrier(GameInterface.getNUMBER_OF_SQUARES_Y(), GameInterface.getNUMBER_OF_SQUARES_X(), GameInterface.getNUMBER_OF_SQUARES_Y());
		barrier2.setVerticalBarrier(GameInterface.getNUMBER_OF_SQUARES_Y(), GameInterface.getNUMBER_OF_SQUARES_X(), GameInterface.getNUMBER_OF_SQUARES_Y());
		boolean achei = false;
		for(int i = 0; i < GameInterface.getNUMBER_OF_SQUARES_Y(); i++) {
			if(gameInterface.barriersComparator(barrier1, barrier2))
				achei = true;
			assertEquals(false, achei);
		}
	}

}
